module.exports = {
  // publicPath: process.env.NODE_ENV === "production" ? "/voronoi-on-gpu" : "/",
  chainWebpack: config => {
    const glslRule = config.module.rule("glsl");
    glslRule
      .test(/\.glsl$/)
      .use("GLSL")
      .loader("webpack-glsl-loader")
      .end();
  }
};
