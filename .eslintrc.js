module.exports = {
  root: true,
  env: {
    node: true
  },
  plugins: ['vuetify'],
  extends: [
    "plugin:vue/essential",
    "eslint:recommended",
    "@vue/typescript"
  ],
  rules: {
    'vuetify/no-deprecated-classes': 'error'
  }
};
