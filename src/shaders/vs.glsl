
// Pass interpolated pixel position to Fragment Shader
varying vec3 pixelPos;

void main() {
  vec4 pos = projectionMatrix * modelViewMatrix * vec4 (position, 1.0);
  pixelPos = position.xyz;
  gl_Position = pos;
}