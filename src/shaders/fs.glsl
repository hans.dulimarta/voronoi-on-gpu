precision mediump float;
precision mediump int;

uniform vec3 seedColors[100];
uniform vec3 seedLocations[100];
uniform int numSeeds;
uniform int metric; // 1:Euclidean 2:Manhattan
varying vec3 pixelPos;

void main() {
    float minDist = 999.0;
    vec3 minColor;
    for (int k = 0; k < 100; ++k) {
        if (k >= numSeeds) break;
        
        float dist;
        float dx;
        float dy;
        float p = float(metric);
        dx = abs(pixelPos.x - seedLocations[k].x);
        dy = abs(pixelPos.y - seedLocations[k].y);
        dist = pow(pow(dx, p) + pow(dy,p), 1.0/p);

        if (dist < minDist) {
            minColor = seedColors[k];
            minDist = dist;
        }
    }
    gl_FragColor = vec4 (minColor, 1.0);
    // if (pixelPos.y < seedLocations[0].y) {
    //     gl_FragColor = vec4 (1,0,0,1);
    // } else if (pixelPos.y < seedLocations[1].y) {
    //     gl_FragColor = vec4 (0,1,0,1);
    // } else {
    //     gl_FragColor = vec4 (0,0,1,1);
    // }
}