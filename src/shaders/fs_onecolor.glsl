precision mediump float;
precision mediump int;

uniform vec2 seedLocations[100];
uniform vec3 seedColors[100];
uniform int numSeeds;
varying vec2 pixelPos;

void main() {
  float minDist = 999.0;
  vec3 minColor;
  for (int k = 0; k < 100; ++k) {
    if (k >= numSeeds) break;
    float dist = distance(pixelPos, seedLocations[k]);
    if (dist < minDist) {
      minColor = seedColors[k];
      minDist = dist;
    }
  }
  gl_FragColor = vec4 (minColor, 1.0);
}