attribute vec2 vertexPosition;
varying vec2 pixelPos;

void main() {
  gl_PointSize = 6.0;
  gl_Position = vec4 (vertexPosition, 0.0, 1.0);
  pixelPos = vertexPosition.xy;
}